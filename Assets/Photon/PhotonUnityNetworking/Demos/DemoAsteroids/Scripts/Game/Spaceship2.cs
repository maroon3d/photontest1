﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Spaceship.cs" company="Exit Games GmbH">
//   Part of: Asteroid Demo,
// </copyright>
// <summary>
//  Spaceship
// </summary>
// <author>developer@exitgames.com</author>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace Photon.Pun.Demo.Asteroids
{
    public class Spaceship2 : MonoBehaviour, IPunObservable
    {
        public float RotationSpeed = 3;
        public float MovementSpeed = 2.0f;
        public float MaxSpeed = 0.2f;
        public float interpolationValue = 1f;

        public GameObject slider;
        public GameObject sliderText;

        private Vector2 joystickDirection;
        private Vector2 joystickDirectionTarget;

        public ParticleSystem Destruction;
        public GameObject EngineTrail;
        public GameObject BulletPrefab;

        private PhotonView photonView;

        private new Rigidbody rigidbody;
        private new Collider collider;
        private new Renderer renderer;

        private float rotation = 0.0f;
        private float acceleration = 0.0f;
        private float shootingTimer = 0.0f;

        private bool controllable = true;
        private bool isDestroyed = false;

        float flexibility = 3.5f;
        float speed = 40f;

        private Vector3 networkPosition;
        private Quaternion networkRotation;

        [HideInInspector]
        public int health = 5;

        #region UNITY

        public void Awake()
        {
            slider = GameObject.Find("Slider");
            sliderText = GameObject.Find("SliderText");

            photonView = GetComponent<PhotonView>();

            rigidbody = GetComponent<Rigidbody>();
            collider = GetComponent<Collider>();
            renderer = GetComponent<Renderer>();
        }

        public void Start()
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                if (r.gameObject.name != "healthBar")
                {
                    r.material.color = AsteroidsGame.GetColor(photonView.Owner.GetPlayerNumber());
                }
                
            }
        }

        public void Update()
        {
            interpolationValue = slider.GetComponent<Slider>().value;
            sliderText.GetComponent<Text>().text = "Lerp: " + slider.GetComponent<Slider>().value.ToString("F2");

            if (!photonView.IsMine || !controllable)
            {
                return;
            }

            //rotation = Input.GetAxis("Horizontal");
            //acceleration = Input.GetAxis("Vertical");

            //joystickDirection = FixedJoystick.fixedJoystick.inputVector;



            if ((FireFF.fireFF.pointerDown || Input.GetKey(KeyCode.Space)) && shootingTimer <= 0.0)
            {
                shootingTimer = 0.1f;

                photonView.RPC("Fire", RpcTarget.AllViaServer, rigidbody.position, rigidbody.rotation);
            }

            if (shootingTimer > 0.0f)
            {
                shootingTimer -= Time.deltaTime;
            }
        }

        public void FixedUpdate()
        {
            if (!photonView.IsMine)
            {
                if (Vector3.Distance(transform.position, networkPosition) < 40f)
                {
                    transform.position = Vector3.Lerp(transform.position, networkPosition, interpolationValue);
                    
                }
                else
                {
                    transform.position = networkPosition;
                }

                transform.rotation = Quaternion.Lerp(transform.rotation, networkRotation, interpolationValue);
                //transform.rotation = networkRotation;
                return;
            }

            if (!controllable)
            {
                return;
            }

            // UPDATE TRANSFORM
            //joystickDirectionTarget = Vector2.Lerp(joystickDirectionTarget, joystickDirection, Time.deltaTime * 3)
            //transform.position += new Vector3(joystickDirectionTarget.x, 0, joystickDirectionTarget.y) * MovementSpeed;
            //transform.LookAt(transform.position + new Vector3(joystickDirectionTarget.x, 0, joystickDirectionTarget.y));

            #region Touch Controller
            

            Vector3 currentDirection = Vector3.forward;
            Vector3 orientationDirection = new Vector3(InputManager.inputManager.orientationDirection.x, 0, InputManager.inputManager.orientationDirection.y);

            transform.Translate(Vector3.forward * speed *  Time.fixedDeltaTime);

            Quaternion lerp = transform.rotation;

            if (InputManager.inputManager.inputOn)
            {
                lerp = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(orientationDirection), flexibility);
            }

            transform.rotation = lerp;
            #endregion

            CheckExitScreen();
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {

            if (stream.IsWriting)
            {
                stream.SendNext(transform.position);
                stream.SendNext(rigidbody.rotation);
                stream.SendNext(transform.forward);
            }
            else
            {
                
                networkPosition = (Vector3)stream.ReceiveNext();
                networkRotation = (Quaternion)stream.ReceiveNext();
                Vector3 vec = (Vector3)stream.ReceiveNext();

                float lag = Mathf.Abs((float)(PhotonNetwork.Time - info.timestamp));

                networkPosition += vec * lag * speed;


                //Debug.Log("LAG: " + lag);
            }
        }
        #endregion

        #region COROUTINES

        private IEnumerator WaitForRespawn()
        {
            yield return new WaitForSeconds(AsteroidsGame.PLAYER_RESPAWN_TIME);

            photonView.RPC("RespawnSpaceship", RpcTarget.AllViaServer);
        }

        #endregion

        #region PUN CALLBACKS

        [PunRPC]
        public void DestroySpaceship()
        {
            if (!controllable)
            {
                return;
            }

            
            collider.enabled = false;
            renderer.enabled = false;
            this.transform.Find("ShipName").gameObject.SetActive(false);
            this.transform.Find("healthBar").gameObject.SetActive(false);

            controllable = false;

            EngineTrail.SetActive(false);
            Destruction.Play();

            if (photonView.IsMine)
            {
                object lives;
                if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(AsteroidsGame.PLAYER_LIVES, out lives))
                {
                    PhotonNetwork.LocalPlayer.SetCustomProperties(new Hashtable { { AsteroidsGame.PLAYER_LIVES, ((int)lives <= 1) ? 0 : ((int)lives - 1) } });
                    Debug.Log(AsteroidsGame.PLAYER_LIVES);

                    if (((int)lives) > 1)
                    {
                        StartCoroutine("WaitForRespawn");
                    }
                }
            }
        }

        [PunRPC]
        public void Fire(Vector3 position, Quaternion rotation, PhotonMessageInfo info)
        {
            float lag = (float)(PhotonNetwork.Time - info.timestamp);
            GameObject bullet;

            ///** Use this if you want to fire one bullet at a time **/
            bullet = Instantiate(BulletPrefab, transform.position, Quaternion.identity) as GameObject;
            bullet.GetComponent<Bullet>().InitializeBullet(photonView.Owner, (rotation * Vector3.forward), Mathf.Abs(lag));


            ///** Use this if you want to fire two bullets at once **/
            //Vector3 baseX = rotation * Vector3.right;
            //Vector3 baseZ = rotation * Vector3.forward;

            //Vector3 offsetLeft = -1.5f * baseX - 0.5f * baseZ;
            //Vector3 offsetRight = 1.5f * baseX - 0.5f * baseZ;

            //bullet = Instantiate(BulletPrefab, rigidbody.position + offsetLeft, Quaternion.identity) as GameObject;
            //bullet.GetComponent<Bullet>().InitializeBullet(photonView.Owner, baseZ, Mathf.Abs(lag));
            //bullet = Instantiate(BulletPrefab, rigidbody.position + offsetRight, Quaternion.identity) as GameObject;
            //bullet.GetComponent<Bullet>().InitializeBullet(photonView.Owner, baseZ, Mathf.Abs(lag));
        }

        [PunRPC]
        public void LifeLose()
        {
            health--;
        }

        [PunRPC]
        public void RespawnSpaceship()
        {
            health = 5;
            collider.enabled = true;
            renderer.enabled = true;
            this.transform.Find("ShipName").gameObject.SetActive(true);
            this.transform.Find("healthBar").gameObject.SetActive(true);

            controllable = true;

            EngineTrail.SetActive(true);
            Destruction.Stop();
        }

        #endregion

        private void CheckExitScreen()
        {
            if (Camera.main == null)
            {
                return;
            }

            if (Mathf.Abs(transform.position.x) > (Camera.main.orthographicSize * Camera.main.aspect))
            {
                transform.position = new Vector3(-Mathf.Sign(transform.position.x) * Camera.main.orthographicSize * Camera.main.aspect, 0, transform.position.z);
                transform.position -= transform.position.normalized * 0.1f; // offset a little bit to avoid looping back & forth between the 2 edges 
            }

            if (Mathf.Abs(transform.position.z) > Camera.main.orthographicSize)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, -Mathf.Sign(transform.position.z) * Camera.main.orthographicSize);
                transform.position -= transform.position.normalized * 0.1f; // offset a little bit to avoid looping back & forth between the 2 edges 
            }
        }

        #region On Collision Enter
        public void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Bullet"))
            {
                if (other.GetComponent<Bullet>().Owner != photonView.Owner)
                {
                    this.gameObject.GetComponent<PhotonView>().RPC("LifeLose", RpcTarget.All);
                    if (health <= 0)
                    {
                        DestroyPlayerGlobally();
                    }
                }
            }
        }

        private void DestroyPlayerGlobally()
        {
            this.gameObject.GetComponent<PhotonView>().RPC("DestroySpaceship", RpcTarget.All);
        }
        #endregion
    }
}