﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireFF : MonoBehaviour
{
    public static FireFF fireFF;
    public bool pointerDown;

	// Use this for initialization
	void Start () {
        fireFF = this;
        pointerDown = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PointerDownEvent()
    {
        pointerDown = true;
    }

    public void PointerUpEvent()
    {
        pointerDown = false;
    }
}
