﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;


namespace Photon.Pun.Demo.Asteroids
{
    public class ShipName : MonoBehaviour {

        // Use this for initialization
        void Start() {

            this.GetComponent<TextMesh>().text = this.transform.parent.GetComponent<PhotonView>().Owner.NickName.ToString();
            this.GetComponent<TextMesh>().color = AsteroidsGame.GetColor(this.transform.parent.gameObject.GetComponent<PhotonView>().Owner.GetPlayerNumber());
            //Debug.Log(transform.parent.name);
        }

        // Update is called once per frame
        void Update() {
            this.transform.position = this.transform.parent.position + new Vector3(0, 0, 10);
            this.transform.eulerAngles = new Vector3(90, 0, 0);
        }
    }
}
