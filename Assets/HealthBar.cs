﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Photon.Pun.Demo.Asteroids
{
    public class HealthBar : MonoBehaviour
    {

        public Gradient gradient;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            int health = this.transform.parent.GetComponent<Spaceship2>().health;

            this.GetComponent<SpriteRenderer>().color = gradient.Evaluate((float)health / 5);
            this.GetComponent<SpriteRenderer>().size = new Vector2(13 * (health / 5f) + 2, 1.2f);

            this.transform.position = this.transform.parent.position + new Vector3(0, 0, 6);
            this.transform.eulerAngles = new Vector3(90, 0, 0);
        }
    }
}
