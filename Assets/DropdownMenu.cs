﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class DropdownMenu : MonoBehaviour {

    public static DropdownMenu dm;

    Dropdown dropDown;
    int value;
    public string txt;

    public ServerSettings serverSettings;

	// Use this for initialization
	void Start () {
        dm = this;
        dropDown = this.GetComponent<Dropdown>();
        dropDown.value = 4;
    }
	
	// Update is called once per frame
	void Update () {
        value = dropDown.value;
        ChangeText();
        //Debug.Log(txt);
        serverSettings.AppSettings.FixedRegion = txt;
    }

    void ChangeText()
    {
        switch (value)
        {
            case 0:
                txt = "asia";
                break;
            case 1:
                txt = "au";
                break;
            case 2:
                txt = "cae";
                break;
            case 3:
                txt = "cn";
                break;
            case 4:
                txt = "eu";
                break;
            case 5:
                txt = "in";
                break;
            case 6:
                txt = "jp";
                break;
            case 7:
                txt = "ru";
                break;
            case 8:
                txt = "rue";
                break;
            case 9:
                txt = "sa";
                break;
            case 10:
                txt = "kr";
                break;
            case 11:
                txt = "us";
                break;
            case 12:
                txt = "usw";
                break;
            default:
                txt = "eu";
                break;
        }
    }
}
