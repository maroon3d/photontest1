﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Photon.Pun.Demo.Asteroids
{
    public class UI1 : MonoBehaviour {

        public GameObject txt;
        public GameObject sphere;

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {
            txt.GetComponent<RectTransform>().anchoredPosition = Camera.main.WorldToScreenPoint(sphere.transform.position);

        }
    }
}
