﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    public static InputManager inputManager;

    public Vector2 orientationDirection { get; private set; }
    public bool inputOn { get; private set; }
    Vector2 fingerPosition;
    Vector2 touchDownPosition;

    // Use this for initialization
    void Awake()
    {
        inputManager = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (Input.mousePosition.x < Screen.width * 0.7f)
                {
                    touchDownPosition = Input.mousePosition;
                    inputOn = true;
                }
            }
            else if (Input.GetMouseButton(0))
            {
                if (Input.mousePosition.x < Screen.width * 0.7f)
                {
                    fingerPosition = Input.mousePosition;
                    orientationDirection = (fingerPosition - touchDownPosition).normalized;
                    inputOn = true;
                }
            }

            


        }
        else
        {
            inputOn = false;
            Touch touch = Input.touches[0];

            if (touch.phase == TouchPhase.Began)
            {
                if (touch.position.x < Screen.width * 0.7f)
                {
                    touchDownPosition = touch.position;
                    inputOn = true;
                }
            }
            else if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
            {
                if (touch.position.x < Screen.width * 0.7f)
                {
                    fingerPosition = touch.position;
                    orientationDirection = (fingerPosition - touchDownPosition).normalized;
                    inputOn = true;
                }
            }
        }
    }
}
