﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon;

namespace Photon.Pun.Demo.Asteroids
{
    public class Host : MonoBehaviour {

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {
            this.GetComponent<Text>().text = "Master Client: " + PhotonNetwork.MasterClient.NickName
                + "\n" + "Total Players: " + PhotonNetwork.PlayerList.Length.ToString()
                + "\n" + "Region: " + PhotonNetwork.CloudRegion
                + "\n" + "Lag: " + PhotonNetwork.GetPing();
        }
    }
}
